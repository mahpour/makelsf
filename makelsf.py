#!/usr/bin/python
import argparse
from argparse import RawTextHelpFormatter
import os.path

__version__ = "0.0.1"
__prog_name__ = ""


def file_exist(filename):
    if os.path.isfile(filename):
        return True
    else:
        return False


def run_args():
    usage = """
                   '||             '||     .|'''|  '||''''| 
                    ||              ||     ||       ||  .   
'||),,(|,   '''|.   || //`  .|''|,  ||     `|'''|,  ||''|   
 || || ||  .|''||   ||<<    ||..||  ||      .   ||  ||      
.||    ||. `|..||. .|| \\\.  `|...  .||...|  |...|' .||.     

by Amin Mahpour | Version {0}  
Create IBM LSF files with ease!
""".format(__version__)

    # Instantiate the parser
    parser = argparse.ArgumentParser(prog=__prog_name__, description=usage, formatter_class=RawTextHelpFormatter)

    parser.add_argument('-v' , '--version', action='version', version='{version}'.format(version=__version__),
                        help='Get version number of this software.')

    # Required positional argument
    parser.add_argument('job_name', type=str,
                        help='The job name is required.')

    parser.add_argument('job_queue', type=str,
                        help='AN IBM LSF compatible job queue is required. \n[short | long | big | big_multi]')

    # Optional positional argument
    #parser.add_argument('opt_pos_arg', type=int, nargs='?', help='An optional integer positional argument')

    # Optional argument
    parser.add_argument('--mem', type=int, help='Memory allocation in GB (default: 8GB).', default="8")
    parser.add_argument('--t', type=int, help='Number of threads to run the job (default: 2).', default="2")


    # Switch
    #parser.add_argument('--switch', action='store_true', help='A boolean\nswitch')

    args = parser.parse_args()


    #if file_exist(args.pos_arg):
    #    print("EXIST!")
    #else:
    #    parser.error("File named '{0}' not found.".format(args.job_name))


    # if False:
    #
    #     print("--------")
    #     print(parser.print_help())
    #     print("Argument values:")
    #     print(args.pos_arg)
    #     print(args.opt_pos_arg)
    #     print(args.opt_arg)
    #     print(args.switch)

    # if args.pos_arg > 10:
    #    parser.error("pos_arg cannot be larger than 10")

    return args


def run_job(args):
    job = '''#!/bin/bash
    #BSUB -J {0}
    #BSUB -o {0}-%J.out
    #BSUB -e {0}-%J.err
    #BSUB -q {1}
    #BSUB -n {2}
    #BSUB -M {3}000
    #BSUB -R rusage[mem={3}000]
    
    echo '---THIS LSF FILE WAS BROUGHT TO YOU BY: MAKELSF PROGRAM, ENJOY!---'
    
    # Some important variables to check (Can be removed later)
    echo '---PROCESS RESOURCE LIMITS---'
    ulimit -a
    echo '---SHARED LIBRARY PATH---'
    echo $LD_LIBRARY_PATH
    echo '---APPLICATION SEARCH PATH:---'
    echo $PATH
    echo '---LSF Parameters:---'
    printenv | grep '^LSF'
    echo '---LSB Parameters:---'
    printenv | grep '^LSB'
    echo '---LOADED MODULES:---'
    module list
    echo '---SHELL:---'
    echo $SHELL
    echo '---HOSTNAME:---'
    hostname
    echo '---GROUP MEMBERSHIP (files are created in the first group listed):---'
    groups
    echo '---DEFAULT FILE PERMISSIONS (UMASK):---'
    umask
    echo '---CURRENT WORKING DIRECTORY:---'
    pwd
    echo '---DISK SPACE QUOTA---'
    df .
    echo '---TEMPORARY SCRATCH FOLDER ($TMPDIR):---'
    echo $TMPDIR

    # Add your job command here
    '''.format(args.job_name, args.job_queue, args.t, args.mem)
    print("{0}.lsf was created!".format(args.job_name))

    output = open("{0}.lsf".format(args.job_name), mode="w")
    output.write(job)
    output.close()

def main():
    args = run_args()
    run_job(args)


if __name__ == "__main__":
    main()


